const someList = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function insertList(arr, parent = document.body) {
 parent.innerHTML = `<ul>` + arr.map(elem => `<li>${elem}</li>`).join('') + `</ul>`;
}
insertList(someList);

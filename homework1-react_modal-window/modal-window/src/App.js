import './App.scss';
import React, {Component} from 'react';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';



class App extends Component {

state = {
    isOpen: false,
    isOpenSec: false
}

openModal = () => {
    this.setState({isOpen: true})
}

openModaSec = () => {
    this.setState({isOpenSec: true})
}

closeModal = () => {
    this.setState({isOpen: false})
}

closeModalSec= () => {
    this.setState({isOpenSec: false})
}

    render() {

    return (
        <div className="App">
            <Button text='Open first modal' bgColor='teal' onClick={this.openModal} />
            <Button text='Open second modal' bgColor='navy' onClick={this.openModaSec} />

            {this.state.isOpen && <Modal
                    header='Do you want to delete?'
                    text='Once you delete this file, it won’t be possible to undo this action. 
                    Are you sure you want to delete it?'
                    cross={true}
                    onClick={this.closeModal}
                    actions={[
                        <Button text='ok' className='modalBtn'/>,
                        <Button text='cancel' className='modalBtn' onClick={this.closeModal}/>
                    ]}
            />}

            {this.state.isOpenSec && <Modal
                    header='Do you really-really want this?'
                    text='Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
                    cross={true}
                    onClick={this.closeModalSec}
                    actions={[
                        <Button text='submit' className='modalBtn'/>,
                        <Button text='exit' className='modalBtn' onClick={this.closeModalSec}/>
                    ]}
            />}
                                    
        </div>
    );
  }
}

export default App;


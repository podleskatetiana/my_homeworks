import React, {Component} from 'react';
import PropTypes from 'prop-types';
import * as Icons from '../../icons';

class Icon extends Component {

  render() {
    const {type, color, onClick, className, filled} = this.props
    const IconJSX = Icons[type]

    return (
      <span className='svg-wrapper' onClick={onClick}>
        {IconJSX({
          color: color,
          className: className,
          filled: filled
        })}
      </span>
    )
  }
}

Icon.propTypes = {
  type: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
  filled: PropTypes.bool,
  onClick: PropTypes.func
}

Icon.defaultProps = {
  color: 'gray',
  className: 'svg-class',
  filled: false
}

export default Icon;
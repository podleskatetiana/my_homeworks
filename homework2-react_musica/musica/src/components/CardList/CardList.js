import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';
import './CardList.scss';

class CardList extends Component {

  render()  {
    const {cards} = this.props

    return (
      <section className='cards-container'>
        {cards.map(card => {
          return (
            <Card 
            key={card.setNumber}
            card={card}
            />
          )
        })}
      </section>
    )
  }
}

CardList.propTypes = {
  cards: PropTypes.array
}

CardList.defaultProps = {
  cards: []
}

export default CardList;
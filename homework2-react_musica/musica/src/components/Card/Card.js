import React, { Component } from 'react';
import './Card.scss';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';
import Icon from '../Icon/Icon';
import PropTypes from 'prop-types';

class Card extends Component {

  state = {
    isOpen: false,
    isFavourite: false,
    addToCart: false
  }

  openModal = () => {
    this.setState({ isOpen: true })
  }

  closeModal = () => {
    this.setState({ isOpen: false })
  }

  setItems = (number, key) => {
    let itemList = JSON.parse(localStorage.getItem(key)) || []

    const item = {
      number: number
    }

    itemList.push(item)
    localStorage.setItem(key, JSON.stringify(itemList))
  }

  removeItems = (number, key) => {
    let itemList = JSON.parse(localStorage.getItem(key)) || []

    const removeItem = itemList.find(el => el.number === number)

    const removeIndex = itemList.indexOf(this.removeItem)

    itemList.splice(removeIndex, 1)

    localStorage.setItem(key, JSON.stringify(itemList))

    if (itemList.length === 0) {
      localStorage.removeItem(key)
    }
  }

  setFavourite = (number, key) => {
    if (this.state.isFavourite === false) {
      this.setItems(number, key)
      this.setState({ isFavourite: true })
    } else {
      this.setState({ isFavourite: false })
      this.removeItems(number, key)
    }
  }

  setCart = (number, key) => {
    if (this.setState.addToCart === false) {
      this.setItems(number, key)
      this.setState({ addToCart: true })
      this.setState({ isOpen: false })
    }
  }

  render() {
    const { card } = this.props

    return (
      <React.Fragment>
        <div key={card.setNumber} className='card'>

          <Icon
            onClick={() => this.setFavourite(card.setNumber, 'setFavourite')}
            type="star"
            color="white"
            className="star-class"
            filled={this.state.isFavourite}
          />

          <div className='card__img-wrapper'>
            <img src={card.url} alt='card' className='card__img' />
          </div>

          <div className='card__info'>

            <span className='card__name'>{card.name}</span>
            <span className='card__author'>{card.author}</span>
            <span className='card__price'>Price: {card.price}  $</span>
            <span className='card__release'>Release date: {card.releaseDate}</span>

            <Button text='Add to cart' bgColor='teal' onClick={this.openModal} />

          </div>

        </div>

        {this.state.isOpen && <Modal
          header='Do you want to add to the cart?'
          text='This song will be added to your cart'
          onClick={this.closeModal}
          cross
          actions={
            {
              okButton: () => (<button onClick={() => this.setCart(card.setNumber, 'cartItems')} className='btn btn-ok'>Yes</button>),
              exitButton: () => (<button onClick={this.closeModal} className='btn btn-cancel'>No</button>)
            }
          }
        />}
      </React.Fragment>
    );
  }

}

Card.propTypes = {
  card: PropTypes.object,
}

Card.defaultPrps = {
  card: {}
}

export default Card;
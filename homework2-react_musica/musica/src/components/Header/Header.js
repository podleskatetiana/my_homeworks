import React, {Component} from 'react';
import './Header.scss';

class Header extends Component {

  render() {
    return (
      <header className='header'>
        <div className='logo logo__wrapper'>
          
          <img src="https://avatanplus.com/files/resources/original/5ade4bdacb045162f4584ed0.png"
               alt='logo'
               className='logo__img'
          />
          <h1 className="logo__text">MUZOBOZ</h1>
        </div>
        <div className='navbar'>
            <ul className='navbar__list'>
              <li className='navbar__item'>Charts</li>
              <li className='navbar__item'>News</li>
              <li className='navbar__item'>Favourites</li>
              <li className='navbar__item'>Cart</li>
            </ul>
          </div>
      </header>
    );
  }
}

export default Header;
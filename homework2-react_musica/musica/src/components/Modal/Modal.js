import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './Modal.scss';

class Modal extends Component {
  render() {
    const {header, text, onClick, actions, cross} = this.props

    return (
      <div className='modal-wrapper'>
        <div className='modal-overlay' onClick={onClick}>
          
          <div className='modal'>
            <h3 className='modal__header'>{header}</h3>
            <span>{text}</span>
            {cross && <button className='crossBtn' onClick={onClick}>X</button>}
            <div className='modal__button-wrapper'>
              {actions.okButton()}
              {actions.exitButton()}
            </div>
          </div>

        </div>
      </div>
    )
  }
}

Modal.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  actions: PropTypes.object,
  cross: PropTypes.bool,
  onClick: PropTypes.func
}

Modal.defaultProps = {
  header: 'Default modal header',
  text: 'Default modal text',
  actions: {},
  cross: true
}

export default Modal;
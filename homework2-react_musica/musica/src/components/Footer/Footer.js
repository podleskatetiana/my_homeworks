import React, {Component} from 'react';
import './Footer.scss';

class Footer extends Component {

  render() {
    return (
      <footer className='footer'>
        <span>&copy; 2021 MUZOBOZ. All rights reserved.</span>
        <span>2,853,679 ratings recorded</span>
      </footer>
    )
  }
}

export default Footer;
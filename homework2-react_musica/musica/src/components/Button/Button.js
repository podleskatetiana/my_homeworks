import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './Button.scss';

class Button extends Component {

  render() {
    const {text, bgColor, className, onClick} = this.props

    return (
      <button
      style={{backgroundColor: bgColor}}
      onClick={onClick}
      className={className}
      >
        {text}
      </button>
    )
  }
}

Button.propTypes = {
  text: PropTypes.string,
  bgColor: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func
}

Button.defaultProps = {
  bgColor: 'transparent',
  text: 'Default button text',
  className: 'btn'
}

export default Button;
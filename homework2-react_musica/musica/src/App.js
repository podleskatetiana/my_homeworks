import React, {Component} from 'react';
import './App.scss';
import axios from 'axios';
import Header from './components/Header/Header';
import CardList from './components/CardList/CardList';
import Footer from './components/Footer/Footer';

class App extends Component {

  state = {
    cards: []
  }

  updateCards = (data) => {
    this.setState({cards: data})
  }

  setCart = () => {
    console.log('set cart')
  }

  componentDidMount() {
    axios('http://localhost:3000/songs.json')
    .then(res => {this.updateCards(res.data)})
  }

  render() {
    const {cards} = this.state

    return (
      <div>
        <Header />
        <CardList cards={cards}/>
        <Footer />
      </div>
    );
  }
}

export default App;

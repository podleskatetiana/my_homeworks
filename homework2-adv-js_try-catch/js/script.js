const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    },

];


function makeList() {
    const rootDiv = document.getElementById('root');
    const list = document.createElement('ul');

    function insertList(book) {
        const {author, name, price} = book;
        rootDiv.append(list);

        function checkBook(elem) {
            const wrongValue = Object.keys(elem);
            console.log(elem);

            if(!(wrongValue.includes('author'))) {
                return `Field "author" is missing`
            } else if(!(wrongValue.includes('name'))) {
                return `Field "name" is missing`
            } else if(!(wrongValue.includes('price'))) {
                return `Field "price" is missing`
            }
        }

        if (author && name && price) {
            list.insertAdjacentHTML('afterbegin', `<li>Автор: ${book.author} | Название книги: ${book.name} | Цена: ${book.price} USD</li>`)
        } else {
            throw new Error(checkBook(book));
        }
    }


    books.forEach((element) => {
        try {
            insertList(element);
        } catch (e) {
            console.error(e.message)
        }
    });

}

makeList();
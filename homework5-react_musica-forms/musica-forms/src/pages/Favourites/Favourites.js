import React from 'react';
import Card from '../../components/Card/Card';
import { useSelector } from 'react-redux';
import { getCards } from '../../store/selectors';
import PropTypes from 'prop-types';


const Favourites = (props) => {
   const cards = useSelector(getCards)
   const { toggleFavourite } = props
   const inFavouriteItems = cards.filter(el => el.inFavourite)

   return (
      <React.Fragment>

         <section className='cards-container'>
            {inFavouriteItems.map(item => {
               return (
                  <Card
                     key={item.setNumber}
                     card={item}
                     toggleFavourite={toggleFavourite}
                  />
               )
            })}
         </section>

      </React.Fragment>
   );
}

Favourites.propTypes = {
   cards: PropTypes.array,
   updateCards: PropTypes.func
}

Favourites.defaultProps = {
   cards: []
}

export default Favourites;
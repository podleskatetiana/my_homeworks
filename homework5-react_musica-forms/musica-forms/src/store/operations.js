import axios from 'axios';
import { setCards } from './actions';

const normalizeData = (data) => {

   return data.map(el => {
      const favourites = JSON.parse(localStorage.getItem('setFavourite')) || []
      const cart = JSON.parse(localStorage.getItem('cartItems')) || []

      el.inFavourite = favourites.includes(el.setNumber)
      el.inCart = cart.includes(el.setNumber)

      return el
   })
}

export const getCardsData = () => (dispatch) => {
   axios('/songs.json')
      .then(res => {
         const normalizeArr = normalizeData(res.data)
         dispatch(setCards(normalizeArr))
      })
}

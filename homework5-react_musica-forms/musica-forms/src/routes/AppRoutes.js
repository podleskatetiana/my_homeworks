import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom'
import Cart from '../pages/Cart/Cart';
import CardList from '../pages/Home/CardList';
import Favourites from '../pages/Favourites/Favourites';
import Error404 from '../components/Error404/Error404';
import PropTypes from 'prop-types';

const AppRoutes = (props) => {

   const { toggleFavourite } = props

   return (
      <div>
         <Switch>
            <Redirect exact from='/' to='/home' />
            <Route exact path='/home' render={() => <CardList toggleFavourite={toggleFavourite} />} />
            <Route exact path='/favourites' render={() => <Favourites toggleFavourite={toggleFavourite} />} />
            <Route exact path='/cart' render={() => <Cart toggleFavourite={toggleFavourite} />} />
            <Route path='*' component={Error404} />
         </Switch>
      </div>
   );
}

AppRoutes.propTypes = {
   cards: PropTypes.array,
   updateCards: PropTypes.func
}

AppRoutes.defaultProps = {
   cards: []
}


export default AppRoutes;
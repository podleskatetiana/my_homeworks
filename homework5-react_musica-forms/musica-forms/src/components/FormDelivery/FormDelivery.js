import React from 'react';
import { Form, Formik, Field } from 'formik';
import './FormDelivery.scss';
import { useDispatch, useSelector } from 'react-redux';
import { setCards, setUserInfo } from '../../store/actions';
import { getCards } from '../../store/selectors';
import { validationFormSchema } from './formDeliveryValidation';




const FormDelivery = ({ inCartItems }) => {
   const dispatch = useDispatch()
   const cards = useSelector(getCards)

   const submitForm = (values) => {
      const { firstName, lastName, age, adress, tel } = values

      console.log(`First name: ${firstName}, Last name: ${lastName}, Age: ${age}, Adress: ${adress}, Phone number: ${tel}`)
      console.group(`Your cart is - `)

      inCartItems.forEach((el) => {
         console.log(`Author: ${el.author} Name: ${el.name}`)
      })
      console.groupEnd()
      const newCardsArr = cards.map((el) => {
         el.inCart = false

         return el
      })

      localStorage.removeItem('cartItems')
      dispatch(setCards(newCardsArr))
      dispatch(setUserInfo(values))

      const inputList = document.querySelectorAll('input')
      inputList.forEach(e => e.value = '')
   }

   return (
      <div className='form-delivery'>
         <Formik
            initialValues={{
               firstName: '',
               lastName: '',
               age: '',
               adress: '',
               tel: ''
            }}
            onSubmit={submitForm}
            validationSchema={validationFormSchema}
         >
            {(formikProps) => {
               const { errors, touched } = formikProps

               return (
                  <Form className='form-delivery__form'>
                     <label className='form-delivery__label'>
                        First name
                     <Field
                           type='text'
                           name='firstName'
                           value={formikProps.values.firstName}
                        />
                     </label>
                     {errors.firstName
                        && touched.firstName
                        && <span className='form-error'>{errors.firstName}</span>}


                     <label className='form-delivery__label'>
                        Last name
                     <Field
                           type='text'
                           name='lastName'
                           value={formikProps.values.lastName}
                        />
                     </label>
                     {errors.lastName
                        && touched.lastName
                        && <span className='form-error'>{errors.lastName}</span>}


                     <label className='form-delivery__label'>
                        Age
                     <Field
                           type='text'
                           name='age'
                           value={formikProps.values.age}
                        />
                     </label>
                     {errors.age
                        && touched.age
                        && <span className='form-error'>{errors.age}</span>}


                     <label className='form-delivery__label'>
                        Adress
                     <Field
                           type='text'
                           name='adress'
                           value={formikProps.values.adress}
                        />
                     </label>
                     {errors.adress
                        && touched.adress
                        && <span className='form-error'>{errors.adress}</span>}


                     <label className='form-delivery__label'>
                        Phone number
                     <Field
                           type='text'
                           name='tel'
                           value={formikProps.values.tel}
                        />
                     </label>
                     {errors.tel
                        && touched.tel
                        && <span className='form-error'>{errors.tel}</span>}


                     <div>
                        <button type='submit' className='form-delivery__submit-btn'>Checkout</button>
                     </div>
                  </Form>
               )
            }}

         </Formik>

      </div>
   );
}



export default FormDelivery;
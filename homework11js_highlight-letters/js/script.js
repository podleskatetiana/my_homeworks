document.addEventListener('keydown', ev => {
   document.querySelectorAll('.btn').forEach(btn => {
      if(ev.key.toLocaleLowerCase() === btn.textContent.toLocaleLowerCase()) {
          btn.classList.add('blue');
      } else {
          btn.classList.remove('blue');
      }
   })
});
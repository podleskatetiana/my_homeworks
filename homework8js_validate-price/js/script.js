const inputField = document.querySelector('input');
const modalPrice = document.createElement('div');
modalPrice.className = 'current-price';
const modalError = document.createElement('div');
modalError.className = 'error-price';


inputField.addEventListener('focus', (eventFocus) => {
    eventFocus.target.style.cssText = 'border: 5px solid green';
    modalError.remove();
});

modalPrice.addEventListener('click', ev => {
    modalPrice.remove();
    inputField.value = '';
    modalError.remove();
})


inputField.addEventListener('blur', (eventBlur) => {
    eventBlur.target.style.cssText = 'border: 0.5px solid grey';

    if(document.querySelector('div.current-price') === document.querySelector('.current-price')) {
        modalPrice.insertAdjacentHTML('afterbegin', `<span class="modal-text">Текущая цена: ${inputField.value}</span> <button class="modal-close">x</button>`);
        document.body.prepend(modalPrice);
    } else {
        modalPrice.remove();
    }

    eventBlur.target.style.cssText = 'color: green';

    if(inputField.value < 0) {
        eventBlur.target.style.cssText = 'border: 5px solid red';
        modalError.insertAdjacentHTML('afterbegin', '<span>Please enter correct price</span>');
        modalPrice.remove();
        document.body.prepend(modalError);
    }
});

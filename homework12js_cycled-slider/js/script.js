const slides = document.querySelectorAll('.images-wrapper .slide');
let currentSlide = 0;
let slideInterval = setInterval(nextSlide, 3000);

function nextSlide() {
    slides[currentSlide].className = 'slide';
    currentSlide = (currentSlide+1)%slides.length;
    slides[currentSlide].className = 'slide show';
}

let play = true;
const stopButton = document.querySelector('.stop');
const showButton = document.querySelector('.showAgain');


function stopShow() {
    stopButton.innerHTML = 'Прекратить';
    play = false;
    clearInterval(slideInterval);
}

function playShow() {
    showButton.innerHTML = 'Возобновить показ';
    play = true;
    slideInterval = setInterval(nextSlide, 3000);
}

stopButton.addEventListener('click', function () {
   if(play) {
       stopShow();
   } else {
       playShow();
   }
});

showButton.addEventListener('click', function () {
    if(play) {
        stopShow();
    } else {
        playShow();
    }
});



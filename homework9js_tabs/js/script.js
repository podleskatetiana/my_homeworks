document.querySelector('.tabs').addEventListener('click', function (e) {
    console.log(e);
    if(e.target.className === 'tabs-title') {
        let dataTab = e.target.getAttribute('data-tab-head');
        let tabsTitle = document.getElementsByClassName('tabs-title');
        for (let i = 0; i < tabsTitle.length; i++) {
            tabsTitle[i].classList.remove('active');
        }
        e.target.classList.add('active');
        let tabContent = document.getElementsByClassName('tabs-content-item');
        for (let i = 0; i < tabContent.length; i++) {
            if(dataTab == i) {
                tabContent[i].style.display = 'block';
            } else {
                tabContent[i].style.display = 'none';
            }
        }
    }
});
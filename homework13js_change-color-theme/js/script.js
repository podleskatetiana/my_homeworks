const changeBtn = document.querySelector('.change-btn');
const changeColor = document.querySelectorAll('a, p');

changeColor.forEach((item) => {
    item.style.color = localStorage.getItem('color');
});

changeBtn.addEventListener('click', (e) => {
    changeColor.forEach((item) => {
        if(item.style.color === 'red') {
            item.style.color = 'violet';
            localStorage.setItem('color', 'violet');
        } else {
            item.style.color = 'red';
            localStorage.setItem('color', 'red');
        }
    })
});


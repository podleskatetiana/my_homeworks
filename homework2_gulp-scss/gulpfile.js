const gulp = require('gulp');
const clean = require('gulp-clean');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const imagemin = require('gulp-imagemin');


const path = {
    src: {
        scss: './src/scss/**/*.scss',
        img: './src/img/**/*',
        js: './src/js/**/*.js',
        html: './index.html'
    },
    dist: {
        root: './dist',
        css: './dist/css',
        img: './dist/img',
        js: './dist/js'
    }
}

/** FUNCTIONS **/

const cleanDist = () => (
    gulp.src(path.dist.root, {allowEmpty: true})
        .pipe(clean())
);

const buildStyles = () => (
    gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(concat('styles.min.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream())
);

const buildImg = () => (
    gulp.src(path.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(path.dist.img))
        .pipe(browserSync.stream())

);

const buildJS = () => (
    gulp.src(path.src.js)
        .pipe(concat('scripts.min.js'))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest(path.dist.js))
        .pipe(browserSync.stream())
);

const devServer = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    })
}

/** WATCHER **/

const watch = () => {
    devServer();
    gulp.watch(path.src.scss, buildStyles).on('change', browserSync.reload);
    gulp.watch(path.src.img, buildImg).on('change', browserSync.reload);
    gulp.watch(path.src.js, buildJS).on('change', browserSync.reload);
    gulp.watch(path.src.html).on('change', browserSync.reload);
}

/** TASKS **/

gulp.task('build', gulp.series(
    cleanDist,
    gulp.parallel(
        buildImg,
        buildJS,
        buildStyles
    )
));

gulp.task('dev', watch);
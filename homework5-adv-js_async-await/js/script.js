//
// Создать простую HTML страницу с кнопкой Вычислить по IP.
// По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, получить оттуда IP адрес клиента.
// Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о физическом адресе.
// Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.
// Все запросы на сервер необходимо выполнить с помощью async await.//

//


async function findIP () {
    const ip = await fetch('https://api.ipify.org/?format=json', {
        method: 'GET',
        headers: {
            'Content-type': 'application/json'
        }
    });

    const ipData = await ip.json();

    const location = await fetch(`http://ip-api.com/json/${ipData.ip}?fields=continent,country,regionName,city,district`);

    const locationData = await location.json();
    return locationData;
}

function postLocation(data) {
    document.querySelector('.gotLocation').innerHTML = `
                          <p>Continent: ${data.continent}</p>
                          <p>Country: ${data.country}</p>
                          <p>Region name: ${data.regionName}</p>
                          <p>City: ${data.city}</p>
                          <p>District: ${data.district = data.district === '' ? null : data.district}</p>
`;
}

const btnIp = document.querySelector('.getBtn');

btnIp.addEventListener('click', async (e) => {
    e.preventDefault();
    postLocation(await findIP());
});


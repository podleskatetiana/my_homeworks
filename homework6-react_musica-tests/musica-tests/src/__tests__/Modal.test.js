import { render } from '@testing-library/react';
import Modal from './../components/Modal/Modal';

describe('Testing Modal.js', () => {

    test('Smoke test Modal.js', () => {
        const imitationBtn = {
            okButton: jest.fn(),
            exitButton: jest.fn()
        }
        render(<Modal actions={imitationBtn} />)
    })

    test('Should render with some props', () => {
        const imitationBtn = {
            okButton: jest.fn(),
            exitButton: jest.fn()
        }

        const { getByTestId } = render(<Modal actions={imitationBtn} />)
        const modalWindow = getByTestId('modal-window')
        const modalContainer = getByTestId('modal-container')

        expect(modalWindow).toBeInTheDocument()
        expect(modalContainer).toBeInTheDocument()
    })
})

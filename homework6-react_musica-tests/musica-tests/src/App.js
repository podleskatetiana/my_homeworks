import React, { useEffect } from 'react';
import './App.scss';
import { useDispatch, useSelector } from 'react-redux';
import { getCardItem, getCards, getIsOpen, getIsOpenCart } from './store/selectors';
import { getCardsData } from './store/operations';
import { setCards, setIsOpenModal, setIsOpenModalCart } from './store/actions';
import Header from './components/Header/Header';
import AppRoutes from './routes/AppRoutes';
import Footer from './components/Footer/Footer';
import Modal from './components/Modal/Modal';

const App = () => {
   const cardItem = useSelector(getCardItem)
   const cards = useSelector(getCards)
   const isOpen = useSelector(getIsOpen)
   const isOpenCart = useSelector(getIsOpenCart)
   const dispatch = useDispatch()

   useEffect(() => {
      dispatch(getCardsData())
   }, [dispatch]);

   const setItems = (number, key) => {
      let itemList = JSON.parse(localStorage.getItem(key)) || []
      itemList.push(number)
      localStorage.setItem(key, JSON.stringify(itemList))
   }

   const removeItems = (number, key) => {
      let itemList = JSON.parse(localStorage.getItem(key)) || []
      const removeItem = itemList.find(el => el === number)
      const removeIndex = itemList.indexOf(removeItem)
      itemList.splice(removeIndex, 1)
      localStorage.setItem(key, JSON.stringify(itemList))
   }

   const toggleCart = (number, key) => {
      const newArr = cards.map(elem => {

         if (elem.setNumber === number) {
            elem.inCart = !elem.inCart

            if (elem.inCart) {
               setItems(number, key)
               dispatch(setIsOpenModal(!isOpen))
            } else {
               removeItems(number, key)
               dispatch(setIsOpenModalCart(!isOpenCart))
            }

         }
         return elem

      })
      dispatch(setCards(newArr))
   }


   const toggleFavourite = (number, key) => {
      const newArr = cards.map(elem => {

         if (elem.setNumber === number) {
            elem.inFavourite = !elem.inFavourite

            if (elem.inFavourite) {
               setItems(number, key)
            } else {
               removeItems(number, key)
            }

         }
         return elem

      })
      dispatch(setCards(newArr))
   }

   return (
      <div>

         <Header />
         <AppRoutes toggleFavourite={toggleFavourite} />
         <Footer />

         {isOpen && <Modal
            header='Do you want to add item to the cart?'
            text='This item will be added to the cart'
            onClick={() => dispatch(setIsOpenModal(!isOpen))}
            cross
            actions={
               {
                  okButton: () => (<button onClick={() => toggleCart(cardItem.setNumber, 'cartItems')}
                     className='btn btn-ok'>Yes</button>),
                  exitButton: () => (<button onClick={() => dispatch(setIsOpenModal(!isOpen))}
                     className='btn btn-cancel'>No</button>)
               }
            }
         />}

         {isOpenCart && <Modal
            header='Do you want to remove this item from the cart?'
            text='This item will be removed from the cart'
            onClick={() => dispatch(setIsOpenModalCart(!isOpenCart))}
            cross
            actions={
               {
                  okButton: () => (<button onClick={() => toggleCart(cardItem.setNumber, 'cartItems')}
                     className='btn btn-ok'>Yes</button>),
                  exitButton: () => (<button onClick={() => dispatch(setIsOpenModalCart(!isOpenCart))}
                     className='btn btn-cancel'>No</button>)
               }
            }
         />}

      </div>
   );
}

export default App;
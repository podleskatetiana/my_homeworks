import React from 'react';
import './Modal.scss';
import PropTypes from 'prop-types';

const Modal = (props) => {
   const { header, text, onClick, actions, cross } = props

   return (
      <div className='modal-wrapper' data-testid='modal-window'>
         <div className='modal-overlay' onClick={onClick}>

            <div className='modal' data-testid='modal-container'>
               <h3 className='modal__header'>{header}</h3>
               <span>{text}</span>
               {cross && <button className='cross-btn' onClick={onClick}>X</button>}
               <div className='modal__button-wrapper'>
                  {actions.okButton()}
                  {actions.exitButton()}
               </div>
            </div>

         </div>
      </div>
   );
}

Modal.propTypes = {
   header: PropTypes.string,
   text: PropTypes.string,
   onClick: PropTypes.func,
   actions: PropTypes.object,
   cross: PropTypes.bool
}

Modal.defaultProps = {
   header: 'Default modal header',
   text: 'Default modal text',
   actions: {},
   cross: true
}

export default Modal;
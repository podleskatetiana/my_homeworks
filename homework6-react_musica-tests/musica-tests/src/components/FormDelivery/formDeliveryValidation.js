import * as Yup from 'yup';

export const validationFormSchema = Yup.object().shape({

   firstName: Yup.string()
      .min(2, 'Too short!')
      .max(50, 'Too long!')
      .required('Required'),
   lastName: Yup.string()
      .min(2, 'Too short!')
      .max(50, 'Too long!')
      .required('Required'),
   age: Yup.number()
      .typeError('Spesify the number, please')
      .required('Required'),
   adress: Yup.string()
      .required('Required'),
   tel: Yup.number()
      .typeError('Spesify the number, please')
      .required('Required')
})
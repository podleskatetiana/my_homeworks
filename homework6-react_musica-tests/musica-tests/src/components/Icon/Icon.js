import React from 'react';
import * as Icons from '../../icons';
import PropTypes from 'prop-types';

const Icon = (props) => {
   const { type, className, color, onClick, filled } = props
   const IconJSX = Icons[type]

   return (
      <span onClick={onClick} className={className}>

         {IconJSX({
            className: className,
            color: color,
            filled: filled
         })}

      </span>
   );
}

Icon.propTypes = {
   type: PropTypes.string,
   className: PropTypes.string,
   color: PropTypes.string,
   onClick: PropTypes.func,
   filled: PropTypes.bool
}

Icon.defaultProps = {
   className: 'svg-slass',
   color: 'gray',
   filled: false
}

export default Icon;
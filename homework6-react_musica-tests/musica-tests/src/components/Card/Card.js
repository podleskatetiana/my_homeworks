import React from 'react';
import './Card.scss';
import Button from '../Button/Button';
import Icon from '../Icon/Icon';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { getIsOpen } from '../../store/selectors';
import { setCard, setIsOpenModal, setIsOpenModalCart } from '../../store/actions';

const Card = (props) => {

   const isOpen = useSelector(getIsOpen)

   const dispatch = useDispatch()

   const { card, toggleFavourite } = props

   return (
      <React.Fragment>

         <div key={card.setNumber} className='card'>
            <Icon
               onClick={() => { toggleFavourite(card.setNumber, 'setFavourite') }}
               type='star'
               color='white'
               className='svg-wrapper-star'
               filled={!!card.inFavourite}
            />

            {window.location.pathname === '/cart'
               && card.inCart
               && <Icon
                  onClick={() => {
                     dispatch(setCard(card))
                     dispatch(setIsOpenModalCart(true))
                  }}
                  type='cross'
                  color='white'
                  className='svg-wrapper-cross'
               />}

            <div className='card__img-wrapper'>
               <img src={card.url} alt='cover' className='card__img' />
            </div>

            <div className='card__info'>
               <span className='card__name'>{card.name}</span>
               <span className='card__author'>{card.author}</span>
               <span className='card__price'>Price: {card.price} $</span>
               <span className='card__release'>Release date: {card.releaseDate}</span>

               <Button
                  text={card.inCart ? 'In cart' : 'Add to cart'}
                  bgColor={card.inCart ? 'teal' : 'darkslategray'}
                  onClick={() => {
                     dispatch(setCard(card))
                     dispatch(setIsOpenModal(!isOpen))
                  }}
                  disabled={card.inCart ? true : false}
               />
            </div>
         </div>

      </React.Fragment>
   );
}

Card.propTypes = {
   card: PropTypes.object,
   cards: PropTypes.array,
   updateCards: PropTypes.func
}

Card.defaultProps = {
   card: {},
   cards: []
}

export default Card;
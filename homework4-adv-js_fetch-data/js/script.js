fetch('https://swapi.dev/api/films/', {
    method: 'GET',
    headers: {
    'Content-type': 'application/json'
    }
})
.then(resp => resp.json())
.then(data => data.results.sort((a, b) => b.episode_id - a.episode_id))
.then(episode => episode.forEach((ep) => {
    const film = document.createElement('div');
    const characters = document.createElement('ol');

    characters.textContent = 'Characters:';

    document.body.prepend(film);
    film.append(characters);

    film.insertAdjacentHTML('afterbegin', `<h4>Title: ${ep.title}</h4><h4>Episode: ${ep.episode_id}</h4><h5>Opening: ${ep.opening_crawl}</h5>`);

    ep.characters.forEach(char => {
        return fetch(char).then(res => res.json())
            .then(character => {
                let charItem = document.createElement('li');

                charItem.textContent = character.name;

                characters.append(charItem);
            })
    })
}))



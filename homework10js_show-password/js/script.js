document.querySelector('.password-form').addEventListener('click', function (e) {
    const target = e.target
    if(target.tagName === 'I') {
       target.classList.toggle('fa-eye-slash');
       const previous = target.previousElementSibling;
        console.log(previous);
        if(previous.type === 'password') {
            previous.type = 'text';
        } else {
            previous.type = 'password';
        }
    }
});

document.querySelector('.btn').addEventListener('click',function (e) {
   e.preventDefault();
   const enterPassword = document.querySelector('.enter-password');
    console.log(enterPassword);
    const confirmPassword = document.querySelector('.confirm-password');
    console.log(confirmPassword);

    if(enterPassword.value === confirmPassword.value) {
       alert('You are welcome!');
   } else {
       document.querySelector('.error').innerHTML = "Нужно ввести одинаковые значения";
   }
});
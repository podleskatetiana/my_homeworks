let userNumber = prompt("Enter your number");
while (isNaN(userNumber) || +userNumber !== parseInt(userNumber)) {
    userNumber = prompt("Incorrect number. Enter again")
}
if(userNumber < 5) {
    console.log("Sorry,no numbers");
} else {
    for (let i = 0; i < userNumber; i++) {
        if(i % 5 === 0 && i !== 0) {
            console.log(i);
        }
    }
}
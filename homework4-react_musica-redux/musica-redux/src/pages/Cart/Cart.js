import React from 'react';
import Card from '../../components/Card/Card';
import { useSelector } from 'react-redux';
import { getCards } from '../../store/selectors';
import PropTypes from 'prop-types';

const Cart = (props) => {
   const cards = useSelector(getCards)
   const { toggleFavourite } = props
   const inCartItems = cards.filter(el => el.inCart)

   return (
      <React.Fragment>

         <section className='cards-container'>
            {inCartItems.map(item => {
               return (
                  <Card
                     key={item.setNumber}
                     card={item}
                     toggleFavourite={toggleFavourite}
                  />
               )
            })}
         </section>

      </React.Fragment>
   );
}

Cart.propTypes = {
   cards: PropTypes.array,
   updateCards: PropTypes.func
}

Cart.defaultProps = {
   cards: []
}

export default Cart;
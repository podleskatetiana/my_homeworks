import React from 'react';
import './CardList.scss';
import Card from '../../components/Card/Card';
import { useSelector } from 'react-redux';
import { getCards } from '../../store/selectors';
import PropTypes from 'prop-types';


const CardList = (props) => {
   const cards = useSelector(getCards)
   const { toggleFavourite } = props

   return (
      <div>
         <section className='cards-container'>
            {cards.map(card => {
               return (
                  <Card
                     key={card.setNumber}
                     card={card}
                     toggleFavourite={toggleFavourite}
                  />
               )
            })}
         </section>
      </div>
   );
}

CardList.propTypes = {
   cards: PropTypes.array,
   updateCards: PropTypes.func
}

CardList.defaultProps = {
   cards: []
}

export default CardList;
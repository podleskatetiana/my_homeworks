import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

const Button = (props) => {
   const { text, bgColor, className, onClick, disabled } = props

   return (
      <button
         style={{ backgroundColor: bgColor }}
         className={className}
         onClick={onClick}
         disabled={disabled}
      >
         {text}
      </button>
   )
}

Button.propTypes = {
   text: PropTypes.string,
   className: PropTypes.string,
   bgColor: PropTypes.string,
   onClick: PropTypes.func,
   disabled: PropTypes.bool
}

Button.defaultProps = {
   text: 'Default button text',
   className: 'btn',
   bgColor: 'transparent',
   disabled: false
}

export default Button;
const firstNumber = prompt("Enter first number:");
const secondNumber = prompt("Enter second number:");
const operation = prompt("Enter your operation:");

function makeCalculation(firstNumber, secondNumber, operation) {
    switch (operation) {
        case "+":
            return firstNumber + secondNumber;
        case "-":
            return firstNumber - secondNumber;
        case "*":
            return firstNumber * secondNumber;
        case "/":
            return firstNumber / secondNumber;
        default:
            return 0;
    }

}
let calculationResult = makeCalculation(firstNumber, secondNumber, operation);
console.log(calculationResult);


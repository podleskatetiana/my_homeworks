/** Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
 Создайте геттеры и сеттеры для этих свойств.
 Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
 Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
 Создайте несколько экземпляров объекта Programmer, выведите их в консоль. **/

class Employee {

    constructor(name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}


class Programmer extends Employee {

    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(value) {
        this._lang = value;
    }

    get salary() {
        return this._salary * 3;
    }

    set salary(value) {
        this._salary = value;
    }
}

const programmer = new Programmer('John', 25, 500, 'English');
console.log(programmer);

const programmerStan = new Programmer('Stan', 200, 1000, 'no Earth language');
console.log(programmerStan);

const programmerLeina = new Programmer('Leina', 15, 200, 'Spanish');
console.log(programmerLeina);

const programmerErik = new Programmer('Erik', 40, 800, 'Norsk');
console.log(programmerErik);
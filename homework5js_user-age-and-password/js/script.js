function createNewUser(userFirstName, userLastName, userBirthday) {
    const user = {
        name: userFirstName,
        lastName: userLastName,
        getLogin: function () {
            return (this.name[0] + this.lastName).toLowerCase();
        },
        birthday: userBirthday,
        getAge: function () {
            const today = new Date();
            let birthdayArr = userBirthday.split(".");
            let birthday = new Date(`${birthdayArr[1]} . ${birthdayArr[0]} . ${birthdayArr[2]}`);
            let age = today.getFullYear() - birthday.getFullYear();
            if (today.getMonth() < birthday.getMonth() || (today.getMonth() === birthday.getMonth() && today.getDate() < birthday.getDate())) {
                age--;
            }
            return `user age: ${age}`;
        },
        getPassword: function () {
            const birthdayArr = userBirthday.split(".");
            const birthday = new Date(`${birthdayArr[1]} . ${birthdayArr[0]} . ${birthdayArr[2]}`);
            return (this.name[0]).toUpperCase() + (this.lastName).toLowerCase() + birthday.getFullYear();
        }

        }
    return user;
    }

const newUser = createNewUser(prompt("Enter your name:"), prompt("Enter your last name:"), prompt("Enter your birthdate:", 'dd.mm.yyyy'));
console.log(newUser);
console.log(newUser.getAge());
console.log("user's login:", newUser.getLogin());
console.log("user's password:", newUser.getPassword());

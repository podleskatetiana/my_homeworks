import React, { useState, useEffect } from 'react';
import './App.scss';
import axios from 'axios';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import AppRoutes from './routes/AppRoutes';

const App = () => {

   const [cards, setCards] = useState([])

   const updateCards = (data) => {
      setCards(data)
   }

   const normalizeData = (data) => {
      return data.map(el => {

         const favourites = JSON.parse(localStorage.getItem('setFavourite')) || []
         const cart = JSON.parse(localStorage.getItem('cartItems')) || []

         el.inFavourite = favourites.includes(el.setNumber)
         el.inCart = cart.includes(el.setNumber)

         return el
      })
   }

   useEffect(() => {
      axios('/songs.json')
         .then(res => {
            const normalizeArr = normalizeData(res.data)
            updateCards(normalizeArr)
         })
   }, [])

   return (
      <div>
         <Header />
         <AppRoutes
            cards={cards}
            updateCards={updateCards}
         />
         <Footer />
      </div>
   )
}

export default App;

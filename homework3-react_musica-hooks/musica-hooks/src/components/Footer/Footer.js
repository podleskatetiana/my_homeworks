import React from 'react';
import './Footer.scss'

const Footer = () => {
  return (
    <footer className='footer'>
      <span>&copy; MUZOBOZ. All rights reserved.</span>
      <span>2,853,679 ratings recorded</span>
    </footer>
  )
}

export default Footer;
import React from 'react';
import './Header.scss';
import { NavLink } from 'react-router-dom';

const Header = () => {
  return (
    <header className='header'>
      <div className='logo logo-wrapper'>
        <img src='https://avatanplus.com/files/resources/original/5ade4bdacb045162f4584ed0.png' alt='logo' className='logo__img' />
        <h1 className='logo__text'>MUZOBOZ</h1>
      </div>
      <div className='navbar'>
        <ul className='navbar__list'>
          <li><NavLink className='navbar__item' activeClassName='navbar__item_is-active' to='/home'>Home</NavLink></li>
          <li><NavLink className='navbar__item' activeClassName='navbar__item_is-active' to='/favourites'>Favourites</NavLink></li>
          <li><NavLink className='navbar__item' activeClassName='navbar__item_is-active' to='/cart'>Cart</NavLink></li>
        </ul>
      </div>
    </header>
  )
}

export default Header;
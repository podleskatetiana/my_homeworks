import React, { useState } from 'react';
import './Card.scss';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';
import Icon from '../Icon/Icon';

const Card = (props) => {
   const [isOpen, setIsOpen] = useState(false)
   const [isOpenCart, setIsOpenCart] = useState(false)

   const { cards, card, updateCards } = props

   const toggleModal = () => {
      setIsOpen(!isOpen)
   }

   const toggleModalCart = () => {
      setIsOpenCart(!isOpenCart)
   }

   const setItems = (number, key) => {
      let itemList = JSON.parse(localStorage.getItem(key)) || []

      itemList.push(number)
      localStorage.setItem(key, JSON.stringify(itemList))
   }

   const removeItems = (number, key) => {
      let itemList = JSON.parse(localStorage.getItem(key)) || []
      const removeItem = itemList.find(el => el === number)
      const removeIndex = itemList.indexOf(removeItem)

      itemList.splice(removeIndex, 1)

      localStorage.setItem(key, JSON.stringify(itemList))
   }

   const toggleFavourite = (number, key) => {
      const newArr = cards.map(e => {
         if (e.setNumber === number) {
            e.inFavourite = !e.inFavourite

            if (e.inFavourite) {
               setItems(number, key)
            } else {
               removeItems(number, key)
            }
         }
         return e
      })
      updateCards(newArr)
   }

   const toggleCart = (number, key) => {
      const newArr = cards.map(e => {
         if (e.setNumber === number) {
            e.inCart = !e.inCart
            if (e.inCart) {
               setItems(number, key)
               setIsOpen(!isOpen)
            } else {
               removeItems(number, key)
               setIsOpenCart(!isOpenCart)
            }
         }
         return e
      })
      updateCards(newArr)
   }

   return (
      <React.Fragment>
         <div key={card.setNumber} className='card'>
            <Icon
               onClick={() => toggleFavourite(card.setNumber, 'setFavourite')}
               type='star'
               color='white'
               className='svg-wrapper-star'
               filled={!!card.inFavourite}
            />

            {window.location.pathname === '/cart'
               && card.inCart
               && <Icon
                  onClick={() => setIsOpenCart(true)}
                  type='cross'
                  color='white'
                  className='svg-wrapper-cross'
               />}

            <div className='card__img-wrapper'>
               <img src={card.url} alt='cover' className='card__img' />
            </div>

            <div className='card__info'>
               <span className='card__name'>{card.name}</span>
               <span className='card__author'>{card.author}</span>
               <span className='card__price'>Price: {card.price} $</span>
               <span className='card__release'>Release date: {card.releaseDate}</span>

               <Button
                  text={card.inCart ? 'In cart' : 'Add to cart'}
                  bgColor={card.inCart ? 'teal' : 'darkslategray'}
                  onClick={toggleModal}
                  disabled={card.inCart ? true : false}
               />
            </div>
         </div>

         {isOpen && <Modal
            header='Do you want to add item to the cart?'
            text='This item will be added to your cart'
            onClick={toggleModal}
            cross
            actions={
               {
                  okButton: () => (<button onClick={() => toggleCart(card.setNumber, 'cartItems')}
                     className='btn btn-ok'>Yes</button>),
                  exitButton: () => (<button onClick={toggleModal} className='btn btn-cancel'>No</button>)
               }
            }
         />}

         {isOpenCart && <Modal
            header='Do you want to remove item from the cart?'
            text='This item will be removed from your cart'
            onClick={toggleModalCart}
            cross
            actions={
               {
                  okButton: () => (<button onClick={() => toggleCart(card.setNumber, 'cartItems')}
                     className='btn btn-ok'>Yes</button>),
                  exitButton: () => (<button onClick={toggleModalCart} className='btn btn-cancel'>No</button>)
               }
            }
         />}
      </React.Fragment>
   )

}

Card.propTypes = {
   card: PropTypes.object,
   cards: PropTypes.array,
   updateCards: PropTypes.func
}

Card.defaultProps = {
   card: {},
   cards: []
}

export default Card;
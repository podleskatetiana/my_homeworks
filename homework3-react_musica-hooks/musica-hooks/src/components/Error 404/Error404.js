import React from 'react';

const Error404 = () => {
  return (
    <div>
      <h1>Error 404! Not found</h1>
    </div>
  );
}

export default Error404;
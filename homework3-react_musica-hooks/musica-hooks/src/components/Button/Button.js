import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

const Button = (props) => {
  const { text, bgColor, className, onClick, disabled } = props

  return (
    <button
      style={{ backgroundColor: bgColor }}
      className={className}
      onClick={onClick}
      disabled={disabled}
    >
      {text}
    </button>
  );
};

Button.propTypes = {
  text: PropTypes.string,
  bgColor: PropTypes.string,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func
}

Button.defaultProps = {
  text: 'Button text',
  bgColor: 'transparent',
  className: 'btn',
  disabled: false
}

export default Button;
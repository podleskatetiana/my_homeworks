import React from 'react';
import * as Icons from '../../icons';
import PropTypes from 'prop-types';

const Icon = (props) => {
  const { type, color, className, onClick, filled } = props
  const IconJSX = Icons[type]

  return (
    <span onClick={onClick} className={className}>
      {IconJSX({
        color: color,
        className: className,
        filled: filled
      })}
    </span>
  )
}

Icon.propTypes = {
  type: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
  filled: PropTypes.bool,
  onClick: PropTypes.func
}

Icon.defaultProps = {
  color: 'gray',
  className: 'svg-class',
  filled: false
}

export default Icon;
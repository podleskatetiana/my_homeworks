import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Favourites from '../pages/Favourites/Favourites';
import Cart from '../pages/Cart/Cart';
import CardList from '../pages/Home/CardList';
import Error404 from '../components/Error 404/Error404';
import PropTypes from 'prop-types';


const AppRoutes = (props) => {
  const { cards, updateCards } = props

  return (
    <div>
      <Switch>
        <Redirect exact from='/' to='/home' />
        <Route exact path='/home' render={() => <CardList cards={cards} updateCards={updateCards} />} />
        <Route exact path='/favourites' render={() => <Favourites cards={cards} updateCards={updateCards} />} />
        <Route exact path='/cart' render={() => <Cart cards={cards} updateCards={updateCards} />} />
        <Route path='*' component={Error404} />
      </Switch>
    </div>
  )
}

AppRoutes.propTypes = {
  cards: PropTypes.array,
  updateCards: PropTypes.func
}

AppRoutes.defaultProps = {
  cards: []
}

export default AppRoutes;
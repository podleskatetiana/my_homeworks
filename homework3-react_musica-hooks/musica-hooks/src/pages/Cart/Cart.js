import React from 'react';
import Card from '../../components/Card/Card';
import PropTypes from 'prop-types';

const Cart = (props) => {

   const { cards, updateCards } = props

   const inCartItems = cards.filter(e => e.inCart)

   return (
      <React.Fragment>

         <section className='cards-container'>
            {inCartItems.map(item => {
               return (
                  <Card
                     key={item.setNumber}
                     card={item}
                     cards={cards}
                     updateCards={updateCards}
                  />
               )
            })}
         </section>

      </React.Fragment>
   )
}

Cart.propTypes = {
   cards: PropTypes.array,
   updateCards: PropTypes.func
}

Cart.defaultProps = {
   cards: []
}

export default Cart;
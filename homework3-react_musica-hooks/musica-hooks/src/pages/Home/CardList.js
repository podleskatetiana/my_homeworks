import React from 'react';
import Card from '../../components/Card/Card';
import './CardList.scss';
import PropTypes from 'prop-types';

const CardList = (props) => {

  const { cards, updateCards } = props

  return (
    <div>
      <section className='cards-container'>
        {cards.map(card => {
          return (
            <Card
              key={card.setNumber}
              card={card}
              cards={cards}
              updateCards={updateCards}
            />
          )
        })}
      </section>
    </div>
  )
}

CardList.propTypes = {
  cards: PropTypes.array,
  updateCards: PropTypes.func
}

CardList.defaultProps = {
  cards: []
}

export default CardList;
import React from 'react';
import Card from '../../components/Card/Card';
import PropTypes from 'prop-types';

const Favourites = (props) => {

   const { cards, updateCards } = props
   console.log(cards);

   const favouriteItems = cards.filter(e => e.inFavourite)

   return (
      <React.Fragment>

         <section className='cards-container'>
            {favouriteItems.map(item => {
               return (
                  <Card
                     key={item.setNumber}
                     card={item}
                     cards={cards}
                     updateCards={updateCards}
                  />
               )
            })}
         </section>

      </React.Fragment>
   )
}

Favourites.propTypes = {
   cards: PropTypes.array,
   updateCards: PropTypes.func
}

Favourites.defaultProps = {
   cards: []
}

export default Favourites;
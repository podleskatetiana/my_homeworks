function filterBy(arr, dataType) {
    return arr.filter(function (el){
        return typeof el === dataType;
    })
}

const filteredArr = filterBy([12, NaN, null, "12", "hello", NaN, 156, true], "number");
console.log(filteredArr);